//
//  TabBarController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 17.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "TabBarController.h"

#import <AATKit/AATKit.h>
#import "InterstitialViewController.h"
#import <AppTrackingTransparency/ATTrackingManager.h>
#import "AppDelegate.h"

@interface TabBarController () <UITabBarDelegate>
@end

@implementation TabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBar.translucent = true;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{

    if (@available(iOS 14, *)) {
        [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
            NSLog(@"done");
        }];
    } else {
        // Fallback on earlier versions
    };
    });
    
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (delegate.needsToDisplayCMP) {
        [delegate.consent showIfNeeded:self];
        delegate.needsToDisplayCMP = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
    
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    // Try to show fullscreen placement when tapping on tab bar items
    [AATKit showPlacement:[InterstitialViewController sharedInstance].interstitialPlacement];
}

@end
