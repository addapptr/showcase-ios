//
//  AppDelegate.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 15.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AATKit/AATKit.h>
#import <AppTrackingTransparency/ATTrackingManager.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AATManagedConsent *consent;
@property (strong, nonatomic) AATCMPGoogle *cmp;
//@property (strong, nonatomic) AATCMPOgury *cmp;
@property (strong) NSNumber *cmpIsDisplaying;
@property BOOL needsToDisplayCMP;
@property (strong) NSNumber *BACKEND_SWITCH_ATF_ON;
@end

