//
//  ShowcaseUITests.swift
//  ShowcaseUITests
//
//  Created by Mahmoud Amer on 07.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

import XCTest
import UIKit

class ShowcaseUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
        snapshot("1", timeWaitingForIdle: 2)
        sleep(2)
        
        agreeCMPAndLocation(app: app)
        //Native Ads
        app.tabBars.buttons["Native Ad"].tap()
        closeFullscreenAd(app: app)
        sleep(2)
        snapshot("NativeAds", timeWaitingForIdle: 0)
        
        //Banners
        app.tabBars.buttons["Banner"].tap()
        snapshot("Banners-Home", timeWaitingForIdle: 0)
        multiSizeBanner(app: app)
        stickyBanner(app: app)
        infeedBanner(app: app)
        
        //Rewarded Video
        sleep(1)
        app.tabBars.buttons["Rewarded Video"].tap()
        snapshot("RewardedVideo-Landing", timeWaitingForIdle: 0)
        sleep(1)
        rewardedVideo(app: app)
        
    }
    
    fileprivate func multiSizeBanner(app: XCUIApplication) {
        app.tables.staticTexts["Multi-Size Banner"].tap()
        sleep(1)
        snapshot("MultiSize-Banners", timeWaitingForIdle: 0)
        app.swipeUp()
        sleep(1)
        app.navigationBars["BannerMultiSizeTableView"].buttons["Banner"].tap()
        sleep(1)
    }
    
    fileprivate func stickyBanner(app: XCUIApplication) {
        app.tables.staticTexts["Sticky Banner"].tap()
        sleep(1)
        snapshot("Sticky-Banners", timeWaitingForIdle: 0)
        app.swipeUp()
        sleep(1)
        app.navigationBars["BannerStickyTableView"].buttons["Banner"].tap()
        sleep(1)
    }
    
    fileprivate func infeedBanner(app: XCUIApplication) {
        app.tables.staticTexts["Banner in Content-Feed"].tap()
        sleep(1)
        snapshot("Infeed-Banners", timeWaitingForIdle: 0)
        app.swipeUp()
        app.swipeUp()
        app.swipeUp()
        app.swipeUp()
        sleep(1)
        app.navigationBars["BannerInContentFeedTableView"].buttons["Banner"].tap()
        sleep(1)
    }
    
    func rewardedVideo(app: XCUIApplication) {
        
        while !app.staticTexts["Rewarded video is ready"].exists {
            print("Rewarded Video still loading, Wait")
        }
        app.buttons["Show Rewarded Video"].tap()
        sleep(1)
        snapshot("Playing-rewarded-video", timeWaitingForIdle: 0)
        sleep(8)
    }
    
    func agreeCMPAndLocation(app: XCUIApplication) {
        //CMP
        tapButton(label: "Agree", app: app)
        //Location
        sleep(5)
        tapButton(label: "Allow", app: app)
        //Location
    }
    
    func tapButton(label: String, app: XCUIApplication) {
        let appButtons = app.buttons
        print("App : \(app.debugDescription)")
        let button = appButtons[label]
        
        print("button exists \(button.exists)")
        if button.exists {
            while !button.isEnabled {
                print("🛑 button disabled, Wait until being enabled 🛑")
            }
            print("👆 Will tap button with label:  \(label) 👆")
            button.tap()
        }
    }
    
    func closeFullscreenAd(app: XCUIApplication) {
        let appButtons = app.buttons
        print("👽 App : \(app.debugDescription)")
        print("👽 App Buttons: \(appButtons.debugDescription)")
        tapButton(label: "Close Advertisement", app: app)
    }
    
    
    func closeAdButtonAction(button: XCUIElement) {
        
    }
    
    func waitForElementToAppear(_ element: XCUIElement, timeInterval: TimeInterval, completion: @escaping ((Bool) -> Void)) {
        let expectation = XCTestExpectation(description: "Wait first fullscreen ad")

        DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval) {
            OperationQueue.main.addOperation {
                expectation.fulfill()
                let webView = XCUIApplication().webViews.element
                completion(webView.isHittable)
            }
        }
        wait(for: [expectation], timeout: 3)
    }
}
