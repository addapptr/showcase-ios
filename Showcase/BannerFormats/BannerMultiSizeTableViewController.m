//
//  BannerMultiSizeTableViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 05.09.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this

#import "BannerMultiSizeTableViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "AATPlacementNames.h"
#import "BannerAdWithExpCell.h"
#import "BannerContentCell.h"
#import "BannerExchangeExplanationCell.h"
#import "ExplanationCell.h"
#import "NSIndexPath+AAT.h"
#import "NSMutableArray+Shuffling.h"
#import "AdCellElement.h"
#import "NSUserDefaults+AAT.h"

const int ADS_COUNT = 4;
const int NEWS_CELL_HEIGHT = 186;

@interface BannerMultiSizeTableViewController () <AATKitStatisticsDelegate>

@property UIRefreshControl* refreshControl;

@property (nonatomic, getter=inFeedBannerPlacement) NSObject<AATBannerPlacementProtocol>* inFeedBannerPlacement; //The placement for the in feed banner. The getter is defined further down
@property AATBannerConfiguration* bannerConfig;

typedef void(^AdRequestCompletionHandler)(UIView * _Nullable bannerView, NSError * _Nullable error);

@property (getter=isNewsLoading) BOOL newsLoading;

@property NSMutableArray *dataSourceArray;
@property NSArray *explanations;
@property NSArray* newsArray;

@property NSMutableArray<AdCellElement *> *adsIndexPaths;
@property int fetchedAdsCount;
@property int adsOffset;

@end

@implementation BannerMultiSizeTableViewController

@synthesize refreshControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataSourceArray = [[NSMutableArray alloc] init];
    self.adsIndexPaths = [[NSMutableArray alloc] init];
    self.fetchedAdsCount = 0;
    self.adsOffset = (int)(UIScreen.mainScreen.bounds.size.height / NEWS_CELL_HEIGHT) + 1;

    [self performSelectorInBackground:@selector(loadNews) withObject:nil];
    
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
    
    // Refresh Control
    self.refreshControl = [[UIRefreshControl alloc] init];
    if (@available(iOS 10, *)) {
        self.tableView.refreshControl = self.refreshControl;
    } else {
        [self.tableView addSubview:self.refreshControl];
    }
    [self.refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    //Add a configuration for banner placement (optional)
    self.bannerConfig = [[AATBannerConfiguration alloc] init];

    // You can decide to count the ad spaces manually, otherwise AATKit will
    // count them for you
    self.bannerConfig.manualAdSpaceCounting = YES;

    //Set the insets for the table view, so that the table view starts and ends correctly and
    //the scroll indicator is displayed correctly.
    //
    //Adjust settings for iOS 11 and previous versions,
    //because iOS 11 is recognizing the height of the NavigationBar and the TabBar automatically.
    if (@available(iOS 13.0, *)) {
        self.tableView.automaticallyAdjustsScrollIndicatorInsets = false;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    self.tableView.contentInset = self.tableView.scrollIndicatorInsets;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70;
    
    [self displayAddaptrLogoWithWebsiteRedirect];
        
    NSNotificationCenter* notificationCenter = NSNotificationCenter.defaultCenter;
    [notificationCenter addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        if ([self isViewLoaded] && self.view.window) {
            [self.tableView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)refreshTableView:(UIRefreshControl*)refreshControl
{
    if (self.isNewsLoading) {
        [self.refreshControl endRefreshing];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Loading"
                                                                       message:@"Still loading!"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        return;
    }

    [self clearBanners];
    [self performSelectorInBackground:@selector(loadNews) withObject:nil];
}

- (void)endRefreshing
{
    [self.refreshControl endRefreshing];
}

- (void)displayAddaptrLogoWithWebsiteRedirect {
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];
    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

//multiSizeBannerPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATBannerPlacementProtocol>*) inFeedBannerPlacement
{
    return (NSObject<AATBannerPlacementProtocol>*)[AATKit getPlacementWithName:inFeedBannerPlacement] ?:
    [AATKit createBannerPlacementWithName:inFeedBannerPlacement configuration:self.bannerConfig andStatisticsDelegate:self];
}

//This method ensures that only one multi size banner will be loaded at a time
- (void) loadAdIfNeeded {
    // Create an ad request
    AATAdRequest* adRequest = [[AATAdRequest alloc] initWithViewController:self];
    AATBannerConfiguration *conf = [[AATBannerConfiguration alloc] init];
    conf.manualAdSpaceCounting = YES;
    // Execute the ad request
    [self.inFeedBannerPlacement executeRequest:adRequest completionHandler:[self adRequestCompletionHandler]];
}

- (AdRequestCompletionHandler)adRequestCompletionHandler
{
    return ^(UIView * _Nullable bannerView, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Request could not be executed with error: %@", error);
            [self performSelector:@selector(loadAdIfNeeded) withObject:nil afterDelay:2.0];
            return;
        }
        
        int adIndex = 2;
        int i = 0;
        for (AdCellElement *element in self.adsIndexPaths) {
            if ([element.adView isKindOfClass:[UILabel class]]){
                self.adsIndexPaths[i].adView = bannerView;
                self.fetchedAdsCount ++;
                [self.tableView reloadRowsAtIndexPaths:@[self.adsIndexPaths[i].indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                if (self.fetchedAdsCount < ADS_COUNT)
                    [self loadAdIfNeeded];
                break;
            }
            i ++;
            adIndex += self.adsOffset;
        }
    };
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    for (AdCellElement *element in self.adsIndexPaths) {
        if (element.indexPath == indexPath) {
            BannerAdWithExpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adCell"];
            [cell emptyContainer];
            cell.adView = element.adView;
            [cell updateLayout];

            if (!NSUserDefaults.standardUserDefaults.hideExplanations) {
                cell.expLabel.text = element.explanation;
                [cell.bottomSeparatorView setHidden:NO];
            } else {
                cell.expLabel.text = @"";
                [cell.bottomSeparatorView setHidden:YES];
            }
            return cell;
        }
    }
    NSDictionary *data = [self.dataSourceArray objectAtIndex:indexPath.row];
    BannerContentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"bannerContentCell" forIndexPath:indexPath];
    cell.labelBannerContentCell.text = [data objectForKey:@"body"];
    cell.titleLabel.text = [data objectForKey:@"title"];
    cell.newsImageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[BannerContentCell class]]) {
        [[AATHelperMethods sharedInstance] openWebsiteWithUrl:@"https://www.addapptr.com/news-and-pr"];
    }
}

-(void)loadNews {
    NSString * filePath =[[NSBundle mainBundle] pathForResource:@"NewsMore" ofType:@"json"];
    
    NSError * error;
    NSString* fileContents =[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    
    if(error)
    {
        NSLog(@"Error reading file: %@",error.localizedDescription);
    }
    
    NSDictionary *newsDictionary = (NSDictionary *)[NSJSONSerialization
                                                    JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                                    options:0 error:NULL];
    
    self.newsArray = [newsDictionary objectForKey:@"news"];
    
    [self performSelectorOnMainThread:@selector(finishLoadingNews) withObject:nil waitUntilDone:NO];
}

-(void)finishLoadingNews {
    // append news array to data source
    self.dataSourceArray = [NSMutableArray arrayWithArray:self.newsArray];
    [self prepareExplanation];
}

-(void)prepareExplanation {
    self.explanations = @[
        @{
            @"type": @"exp",
            @"exp": @"The size of a multi-size banner varies, so the size of the cell must vary, too."
        },
        @{
            @"type": @"exp",
            @"exp": @"It will scroll like the rest of the content."
        },
        @{
            @"type": @"exp",
            @"exp": @"It will leave the screen like the rest of the content."
        },
        @{
            @"type": @"exp",
            @"exp": @"Only one banner should be visible."
        }
    ];
    [self reserveAdsIndexPaths];
    
}

- (void)reserveAdsIndexPaths {
    int adIndex = 2;
    for (int i = 0; i < ADS_COUNT; i ++ ) {
        AdCellElement *cellElement = [AdCellElement alloc];
        NSIndexPath *path = [NSIndexPath indexPathForRow:adIndex inSection:0];
        if (self.explanations.count > i)
            cellElement.explanation = self.explanations[i][@"exp"];
        else
            cellElement.explanation = self.explanations[0][@"exp"];
        cellElement.indexPath = path;
        cellElement.adView = [[UILabel alloc] init];
        [self.adsIndexPaths addObject:cellElement];
        adIndex += self.adsOffset;
    }
    [self.tableView reloadData];
    [self loadAdIfNeeded];
    [self endRefreshing];
}

-(void)clearBanners {
    self.fetchedAdsCount = 0;
    [self.dataSourceArray removeAllObjects];
    [self.adsIndexPaths removeAllObjects];
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}
@end





