//
//  BannerStickyTableViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 12.09.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this

#import "BannerStickyTableViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "AATPlacementNames.h"
#import "BannerContentCell.h"
#import "BannerExchangeExplanationCell.h"
#import "BannerLoadingTimes.h"
#import "ExplanationCell.h"
#import "NSIndexPath+AAT.h"
#import "NSMutableArray+Shuffling.h"
#import "NSUserDefaults+AAT.h"

@interface BannerStickyTableViewController () <AATKitStatisticsDelegate>

@property AATKitAdType placementType; //There are different placement types of banners for different widths of the screens
@property UIView* bannerPlacementView; //The loaded ad view will be placed on the bannerPlacementView
@property UIVisualEffectView* viewForBannerPlacementView; //bannerPlacementView will be displayed in viewForBannerPlacementView

@property (nonatomic, getter=stickyBannerPlacement) NSObject<AATKitPlacement>* stickyBannerPlacement; //The placement for the sticky banner. The getter is defined further down

@property NSMutableArray* dataSourceArray;
@property NSArray* newsArray;

@end

@implementation BannerStickyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSourceArray = [[NSMutableArray alloc] init];
    [self performSelectorInBackground:@selector(loadNews) withObject:nil];
    
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70;
    
    //Set view controller for placement
    [AATKit setPlacementViewController:self forPlacement:self.stickyBannerPlacement];
            
    self.tableView.contentInset = self.tableView.scrollIndicatorInsets;
    
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];
    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
        
    //Set placement align to center the banner horizontally in the BannerAdCell
    [AATKit setPlacementAlign:AATKitBannerCenter forPlacement:self.stickyBannerPlacement];
    self.bannerPlacementView = [AATKit getPlacementView:self.stickyBannerPlacement];
    
    //Set up a VisualEffectView with a blurEffect
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    
    self.viewForBannerPlacementView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    //Make it 53 pixels high (in this case a banner with a height of 53 pixels is requested)
    //Make it the width of the screen.
    CGRect viewForBannerPlacementViewFrame = self.viewForBannerPlacementView.frame;
    viewForBannerPlacementViewFrame.size.height = 53;
    viewForBannerPlacementViewFrame.size.width = self.tableView.bounds.size.width;
    viewForBannerPlacementViewFrame.origin.y = self.view.bounds.size.height - viewForBannerPlacementViewFrame.size.height - self.tabBarController.tabBar.bounds.size.height;
    self.viewForBannerPlacementView.frame = viewForBannerPlacementViewFrame;
    
    //Set the frame for the bannerPlacementView (that will be inside viewForBannerPlacementView)
    CGRect bannerPlacementViewFrame = self.bannerPlacementView.frame;
    bannerPlacementViewFrame.origin.y = 0;
    bannerPlacementViewFrame.size.width = 320;
    self.bannerPlacementView.frame = bannerPlacementViewFrame;
    
    NSNotificationCenter* notificationCenter = NSNotificationCenter.defaultCenter;
    [notificationCenter addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        
        if ([self isViewLoaded] && self.view.window) {
            self.dataSourceArray = [[NSMutableArray alloc] init];
            [self performSelectorInBackground:@selector(loadNews) withObject:nil];
        }
    }];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    //Start autoreloading sticky banner placement
    [AATKit startPlacementAutoReloadWithSeconds:stickyBannerLoadingTime forPlacement:self.stickyBannerPlacement];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    
    //Create and set position of sticky banner
    CGPoint pos = CGPointMake(self.bannerPlacementView.frame.origin.x, self.bannerPlacementView.frame.origin.y);
    [AATKit setPlacementPos:pos forPlacement:self.stickyBannerPlacement];
    
    //Add the VisualEffectView to view.window, because like this it will not scroll with the table view
    //This is done in viewDidAppear, because you can't add to view.window before
    [self.view.window addSubview:self.viewForBannerPlacementView];
    
    //Add bannerPlacementView to the VisualEffectView
    [self.viewForBannerPlacementView.contentView addSubview:self.bannerPlacementView];
    
    self.bannerPlacementView.center = CGPointMake(self.view.center.x, self.viewForBannerPlacementView.contentView.center.y);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:true];
    
    //Stop autoreloading, so that AATKit can work faster, if you have other placements in other views.
    [AATKit stopPlacementAutoReload:self.stickyBannerPlacement];
    
    //Remove views from superview, so that they are not twice there next time the view will be loaded.
    [self.viewForBannerPlacementView removeFromSuperview];
    [self.bannerPlacementView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

//stickyBannerPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATKitPlacement>*)stickyBannerPlacement{
    
    //The type of AATKitBanner depends on the screen width of the used device.
    CGRect screenRect   = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if (screenWidth == 414.0) {
        self.placementType = AATKitBanner414x53; // wrapper for 320x53
    }
    else if (screenWidth == 375.0) {
        self.placementType = AATKitBanner375x53; // wrapper for 320x53
    } else {
        self.placementType = AATKitBanner320x53;
    }
    
    return [AATKit getPlacementWithName:stickyBannerPlacement] ?:
    [AATKit createPlacementWithName:stickyBannerPlacement andType:self.placementType andStatisticsDelegate:self];
}

//Calculate how many rows have to be between two cells,
//so that it is not possible to see these two cells both in one screen for a given row height.
-(NSUInteger)calculateAdCellOffsetForRowHeight:(CGFloat)rowHeight{
    
    NSUInteger adCellOffset = (int)(ceil(self.tableView.frame.size.height / rowHeight));
    
    return adCellOffset;
}

//Give back the given row modulo the given row offset.
//The row offset from the method calculateAdCellOffsetForRowHeight is used for the parameter repeatingRowOffset.
- (NSUInteger)calculateRowOffsetForIndexPathRow:(NSUInteger)row withOffset:(NSUInteger)repeatingRowOffset{
    
    row = (row % repeatingRowOffset);
    
    return row;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [self.dataSourceArray objectAtIndex:indexPath.row];
    NSString *type = [data objectForKey:@"type"];
    
    if ([type isEqualToString:@"exp"]) {
        ExplanationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"explanationCell"];
        [cell changeText:[data objectForKey:@"exp"]];
        return cell;
    } else if ([type isEqualToString:@"news"]) {
        BannerContentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"bannerContentCell"];
        cell.labelBannerContentCell.text = [data objectForKey:@"body"];
        cell.titleLabel.text = [data objectForKey:@"title"];
        cell.newsImageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
        return cell;
    }
    return [[UITableViewCell alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[BannerContentCell class]]) {
        [[AATHelperMethods sharedInstance] openWebsiteWithUrl:@"https://www.addapptr.com/news-and-pr"];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)loadNews {
    NSString * filePath =[[NSBundle mainBundle] pathForResource:@"news" ofType:@"json"];
    NSError * error;
    NSString* fileContents =[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    if(error)
        NSLog(@"Error reading file: %@",error.localizedDescription);
    
    NSDictionary *newsDictionary = (NSDictionary *)[NSJSONSerialization
                                                    JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                                    options:0 error:NULL];
    
    self.newsArray = [newsDictionary objectForKey:@"news"];
    
    [self performSelectorOnMainThread:@selector(finishLoadingNews) withObject:nil waitUntilDone:NO];
}

-(void)finishLoadingNews {
    // append news array to data source
    [self.dataSourceArray addObjectsFromArray:self.newsArray];
    if (!NSUserDefaults.standardUserDefaults.hideExplanations) {
        [self fillExplanationsArray];
    } else {
        [self.tableView reloadData];
    }
}

-(void)fillExplanationsArray {
    NSArray *explanations = @[
        @{
            @"type": @"exp",
            @"exp": @"Sticky banners are not allowed to hide any content of the app and always stay at their place so there is always place reserved for them."
        },
        @{
            @"type": @"exp",
            @"exp": @"That’s why you need to predetermine the space of the placeholder based on the banner size you want to display."
        },
        @{
            @"type": @"exp",
            @"exp": [NSString stringWithFormat:@"Trying to exchange banner every %i seconds", stickyBannerLoadingTime]
        }
    ];
    self.dataSourceArray = [self.dataSourceArray injectObjectsWithMax:3 objects:explanations];
    [self.tableView reloadData];
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}

@end
