//
//  AdCellElement.h
//  Showcase
//
//  Created by Mahmoud Amer on 12.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdCellElement: NSObject
@property NSString *explanation;
@property NSIndexPath *indexPath;
@property (nullable) UIView *adView;
@end

NS_ASSUME_NONNULL_END
