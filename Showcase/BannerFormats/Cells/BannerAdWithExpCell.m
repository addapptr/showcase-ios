//
//  BannerAdWithExpCell.m
//  Showcase
//
//  Created by Mahmoud Amer on 08.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import "BannerAdWithExpCell.h"

@implementation BannerAdWithExpCell

-(void)updateLayout {
    if (self.adView) {
        //Remove _adView from superview, even if there is none added.
        [self emptyContainer];
        [self.containerView addSubview:self.adView];
        self.adView.center = CGPointMake(self.containerView.center.x, self.adView.center.y);
        [self addConstraintsToAdView];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self addConstraintsToAdView];
}

-(void)emptyContainer {
    for (UIView *subview in self.containerView.subviews) {
        [subview removeFromSuperview];
    }
}

-(void)addConstraintsToAdView {
    if (self.adView && (self.containerView.subviews.count > 0)) {
        self.adView.center = CGPointMake(self.containerView.center.x, self.adView.center.y);
        NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint
                                                 constraintWithItem:self.adView attribute:NSLayoutAttributeCenterX
                                                 relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:
                                                 NSLayoutAttributeCenterX multiplier:1.0 constant:0];
        
        NSLayoutConstraint *topConstraint = [NSLayoutConstraint
                                             constraintWithItem:self.adView attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:
                                             NSLayoutAttributeTop multiplier:1.0 constant:8];

        NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint
                                             constraintWithItem:self.adView attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual toItem:self.containerView attribute:
                                             NSLayoutAttributeBottom multiplier:1.0 constant:8];

        [self.containerView addConstraints:@[topConstraint, bottomConstraint, centerXConstraint]];
        [self.contentView layoutIfNeeded];
        [self layoutIfNeeded];
    }
}

@end
