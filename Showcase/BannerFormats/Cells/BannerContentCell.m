//
//  BannerContentCell.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 22.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "BannerContentCell.h"
#import "NSUserDefaults+AAT.h"

@interface BannerContentCell()

@end

@implementation BannerContentCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
}

-(void)prepareForReuse {
    [super prepareForReuse];
}

@end
