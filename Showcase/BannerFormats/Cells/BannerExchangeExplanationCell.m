//
//  BannerExchangeExplanationCell.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 18.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "BannerExchangeExplanationCell.h"

@interface BannerExchangeExplanationCell()

@property (weak, nonatomic) IBOutlet UILabel *labelExchangeExplanationCell;

@end

@implementation BannerExchangeExplanationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.labelExchangeExplanationCell.text = @"";
}

-(void)changeText:(NSString*)text{
    self.labelExchangeExplanationCell.text = text;
    
    [self.labelExchangeExplanationCell sizeToFit];
}

@end
