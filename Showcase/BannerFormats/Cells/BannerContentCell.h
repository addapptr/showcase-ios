//
//  BannerContentCell.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 22.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerContentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelBannerContentCell;

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;

@end
