//
//  BannerAdWithExpCell.h
//  Showcase
//
//  Created by Mahmoud Amer on 08.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface BannerAdWithExpCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) UIView *adView;
@property (weak, nonatomic) IBOutlet UILabel *expLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;

-(void)emptyContainer;
-(void)updateLayout;
-(void)addConstraintsToAdView;
@end
NS_ASSUME_NONNULL_END
