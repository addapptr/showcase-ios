//
//  BannerExchangeExplanationCell.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 18.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BannerExchangeExplanationCell : UITableViewCell

-(void)changeText:(NSString*)text;

@end
