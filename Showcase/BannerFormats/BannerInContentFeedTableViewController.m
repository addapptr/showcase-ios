//
//  BannerInContentFeedTableViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 22.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this

#import "BannerInContentFeedTableViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "AATPlacementNames.h"
#import "BannerAdWithExpCell.h"
#import "BannerContentCell.h"
#import "BannerExchangeExplanationCell.h"
#import "BannerLoadingTimes.h"
#import "ExplanationCell.h"
#import "NSIndexPath+AAT.h"
#import "NSMutableArray+Shuffling.h"
#import "AdCellElement.h"
#import "NSUserDefaults+AAT.h"

const int MAX_BANNERS_NUMBER = 4;
const int INFEED_CELL_HEIGHT = 186;

@interface BannerInContentFeedTableViewController () <AATKitStatisticsDelegate>

@property (nonatomic, getter=bannerInContentFeedPlacement, readonly) NSObject<AATKitPlacement>* bannerInContentFeedPlacement; //The placement for the sticky banner. The getter is defined further down
@property AATKitAdType placementType; //There are different placement types of banners for different widths of the screens
@property (strong) UIView *adView;
@property BOOL bannerCurrentlyDisplayed;

@property UILabel* titleLabel;

@property NSMutableArray *dataSourceArray;
@property NSArray *explanations;

@property NSMutableArray<AdCellElement *> *adsIndexPaths;
@property int adsOffset;

@end


@implementation BannerInContentFeedTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
        
    //Set view controller for placement
    [AATKit setPlacementViewController:self forPlacement:self.bannerInContentFeedPlacement];
    //Set placement align to center the banner horizontally in the BannerAdCell
    [AATKit setPlacementAlign:AATKitBannerCenter forPlacement:self.bannerInContentFeedPlacement];
    
    NSNotificationCenter* notificationCenter = NSNotificationCenter.defaultCenter;
    [notificationCenter addObserverForName:@"AATKitHaveAd" object:self.bannerInContentFeedPlacement queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        //Store placement view in adView
        self.adView = [AATKit getPlacementView:self.bannerInContentFeedPlacement];
        [self loopThroughAdsCells];
    }];
    
    [notificationCenter addObserverForName:UIApplicationWillEnterForegroundNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        if ([self isViewLoaded] && self.view.window) {
            [self.tableView reloadData];
        }
    }];
        
    if (@available(iOS 13.0, *)) {
        self.tableView.automaticallyAdjustsScrollIndicatorInsets = false;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    self.tableView.delegate = self;
    self.tableView.contentInset = self.tableView.scrollIndicatorInsets;
    
    [self displayAddaptrLogoWithWebsiteRedirect];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70;
    
    self.dataSourceArray = [[NSMutableArray alloc] init];
    self.adsIndexPaths = [[NSMutableArray alloc] init];
    self.adsOffset = (int)(UIScreen.mainScreen.bounds.size.height / INFEED_CELL_HEIGHT) + 1;
    [self performSelectorInBackground:@selector(loadNews) withObject:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Start autoreloading banner placement
    self.bannerCurrentlyDisplayed = false;
    
    [self.tableView reloadData];
    if (self.adView) {
        [self loopThroughAdsCells];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [AATKit stopPlacementAutoReload:self.bannerInContentFeedPlacement];
}

- (void)loopThroughAdsCells {
    for (UITableViewCell* cell in self.tableView.visibleCells) {
        if ([cell isKindOfClass:[BannerAdWithExpCell class]]) {
            [(BannerAdWithExpCell *)cell emptyContainer];
            ((BannerAdWithExpCell *)cell).adView = self.adView;
            [((BannerAdWithExpCell *)cell) updateLayout];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)displayAddaptrLogoWithWebsiteRedirect {
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];
    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

//bannerInContentFeedPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATKitPlacement>*)bannerInContentFeedPlacement{

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if (screenWidth == 414.0) {
        self.placementType = AATKitBanner414x53; // wrapper for 320x53
    }
    else if (screenWidth == 375.0) {
        self.placementType = AATKitBanner375x53; // wrapper for 320x53
    } else {
        self.placementType = AATKitBanner320x53;
    }
    
    return [AATKit getPlacementWithName:bannerInContentFeedPlacement] ?:
    [AATKit createPlacementWithName:bannerInContentFeedPlacement andType:self.placementType andStatisticsDelegate:self];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    for (AdCellElement *element in self.adsIndexPaths) {
        if (element.indexPath == indexPath) {
            
            BannerAdWithExpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adCell"];
            [cell emptyContainer];
            cell.adView = self.adView;
            [cell updateLayout];
            [cell layoutIfNeeded];
            if (!NSUserDefaults.standardUserDefaults.hideExplanations) {
                cell.expLabel.text = element.explanation;
                [cell.bottomSeparatorView setHidden:NO];
            } else {
                cell.expLabel.text = @"";
                [cell.bottomSeparatorView setHidden:YES];
            }
            return cell;
        }
    }
    NSDictionary *data = [self.dataSourceArray objectAtIndex:indexPath.row];
    NSString *type = [data objectForKey:@"type"];
    if ([type isEqualToString:@"news"]) {
        BannerContentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"bannerContentCell" forIndexPath:indexPath];
        cell.labelBannerContentCell.text = [data objectForKey:@"body"];
        cell.titleLabel.text = [data objectForKey:@"title"];
        cell.newsImageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
        
        return cell;
    }
    return [[UITableViewCell alloc] init];
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[BannerAdWithExpCell class]]) {
        self.bannerCurrentlyDisplayed = NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[BannerContentCell class]]) {
        [[AATHelperMethods sharedInstance] openWebsiteWithUrl:@"https://www.addapptr.com/news-and-pr"];
    }
}

-(void)loadNews {
    NSString * filePath =[[NSBundle mainBundle] pathForResource:@"NewsMore" ofType:@"json"];
    
    NSError * error;
    NSString* fileContents =[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    
    if(error)
    {
        NSLog(@"Error reading file: %@",error.localizedDescription);
    }
    
    NSDictionary *newsDictionary = (NSDictionary *)[NSJSONSerialization
                                                    JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                                    options:0 error:NULL];
    
    self.dataSourceArray = [[newsDictionary objectForKey:@"news"] mutableCopy];
    
    [self performSelectorOnMainThread:@selector(finishLoadingNews) withObject:nil waitUntilDone:NO];
}

-(void)finishLoadingNews {
    [self fillExplanationsArray];
}

-(void)fillExplanationsArray {
    self.explanations = @[
        @"The place of a banner in a content-feed is fixed in a cell of the table.",
        @"It will scroll like the rest of the content.",
        @"It will leave the screen like the rest of the content.",
        @"The same banner should be displayed with scrolling",
        @"In this example the banner is reused for each ad cell",
        [NSString stringWithFormat:@"Trying to exchange banner every %i seconds", bannerInContentFeedLoadingTime]
    ];
    [self reserveAdsIndexPaths];
}

- (void)reserveAdsIndexPaths {
    int adIndex = 2;
    for (int i = 0; i < MAX_BANNERS_NUMBER; i ++ ) {
        AdCellElement *cellElement = [AdCellElement alloc];
        NSIndexPath *path = [NSIndexPath indexPathForRow:adIndex inSection:0];
        if (self.explanations.count > i)
            cellElement.explanation = self.explanations[i];
        else
            cellElement.explanation = self.explanations[0];
        cellElement.indexPath = path;
        cellElement.adView = nil;
        [self.adsIndexPaths addObject:cellElement];
        adIndex += self.adsOffset;
    }
    [self.tableView reloadData];
    [AATKit startPlacementAutoReloadWithSeconds:bannerInContentFeedLoadingTime forPlacement:self.bannerInContentFeedPlacement];
    [AATKit reloadPlacement:self.bannerInContentFeedPlacement forceLoad:YES];
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}

@end
