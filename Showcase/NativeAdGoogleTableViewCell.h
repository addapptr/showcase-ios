//
//  NativeAdGoogleTableViewCell.h
//  Showcase
//
//  Created by Mahmoud Amer on 13.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AATKit/AATKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NativeAdGoogleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *explanationLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;

@property (nonatomic, nullable) NSObject<AATKitNativeAd>* ad;
- (void)updateContentForAd:(NSObject<AATKitNativeAd>*)ad;
- (void)clearAd;
@end

NS_ASSUME_NONNULL_END
