//
//  InterstitialViewController.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 16.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

@interface InterstitialViewController : UIViewController
+ (InterstitialViewController*)sharedInstance;
@property (assign, nonatomic, getter=interstitialPlacement) NSObject<AATKitPlacement>* interstitialPlacement;
@property BOOL startupInterstitialHasBeenDisplayed;
@end
