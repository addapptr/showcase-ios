//
//  InterstitialViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 16.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

/**
 ** The placement, that is created here will be shown if you tap the tab bar,
 ** so please take a look at the TabBarController, too.
 **/

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this

#import "InterstitialViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "AATPlacementNames.h"
#import "NSUserDefaults+AAT.h"



@interface InterstitialViewController () <AATKitStatisticsDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelInterstitialText;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceNotIOS11;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceIOS11;

@property NSUserDefaults *defaults;
@property BOOL hideExplanations;

@property BOOL viewDidLoadOneTime;

@end

@implementation InterstitialViewController

+ (InterstitialViewController*)sharedInstance
{
    static InterstitialViewController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
    
    //Set view controller for placement.
    [AATKit setPlacementViewController:self forPlacement:self.interstitialPlacement];
    [AATKit startPlacementAutoReload:self.interstitialPlacement];
    
    self.defaults = NSUserDefaults.standardUserDefaults;
    self.hideExplanations = self.defaults.hideExplanations;
    
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];
    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
    
    if (!self.hideExplanations) {
        NSString* text;
        if (InterstitialViewController.sharedInstance.startupInterstitialHasBeenDisplayed) {
            text = @"This was a startup interstitial.\n\nA startup interstitial has to be displayed before the content of the app.\n\n\n\nThe automatic reloading for the intersitial placement has been started.\n\nYou can try to display the interstitial by tapping on the tabs of the tab bar.\n\nWhen an interstitial is displayed, it fills the whole screen and interrupts/pauses your app.\n\nClose the interstitial to return to the app.\n\nYou can determine a frequency capping for an interstitial placement, which defines the time until another interstitial can be shown.";
        } else {
            text = @"The automatic reloading for the intersitial placement has been started.\n\nYou can try to display the interstitial by tapping on the tabs of the tab bar.\n\nWhen an interstitial is displayed, it fills the whole screen and interrupts/pauses your app.\n\nClose the interstitial to return to the app.\n\nYou can determine a frequency capping for an interstitial placement, which defines the time until another interstitial can be shown.";
        }
        self.labelInterstitialText.text = text;
        [self.labelInterstitialText sizeToFit];
    } else {
        NSString* text = @"Tap the tabs of the tab bar for trying to display an interstitial.";
        self.labelInterstitialText.text = text;
        [self.labelInterstitialText sizeToFit];
    }
    
    NSNotificationCenter* notificationCenter = NSNotificationCenter.defaultCenter;
    [notificationCenter addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        
        self.hideExplanations = self.defaults.hideExplanations;
        
        if (self.viewDidLoadOneTime) {
            
            if (self.hideExplanations) {
                NSString* text = @"Tap the tabs of the tab bar for trying to display an interstitial.";
                self.labelInterstitialText.textAlignment = NSTextAlignmentLeft;
                self.labelInterstitialText.text = text;
            } else {
                NSString* text = @"The automatic reloading for the intersitial placement has been started.\n\nYou can try to display the interstitial by tapping on the tabs of the tab bar.\n\nWhen an interstitial is displayed, it fills the whole screen and interrupts/pauses your app.\n\nClose the interstitial to return to the app.\n\nYou can determine a frequency capping for an interstitial placement, which defines the time until another interstitial can be shown.";
                self.labelInterstitialText.textAlignment = NSTextAlignmentLeft;
                self.labelInterstitialText.text = text;
            }
        }
    }];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.hideExplanations = self.defaults.hideExplanations;
    
    if (self.viewDidLoadOneTime) {
        
        if (self.hideExplanations) {
            NSString* text = @"Tap the tabs of the tab bar for trying to display an interstitial.";
            self.labelInterstitialText.textAlignment = NSTextAlignmentLeft;
            self.labelInterstitialText.text = text;
        } else {
            NSString* text = @"The automatic reloading for the intersitial placement has been started.\n\nYou can try to display the interstitial by tapping on the tabs of the tab bar.\n\nWhen an interstitial is displayed, it fills the whole screen and interrupts/pauses your app.\n\nClose the interstitial to return to the app.\n\nYou can determine a frequency capping for an interstitial placement, which defines the time until another interstitial can be shown.";
            self.labelInterstitialText.textAlignment = NSTextAlignmentLeft;
            self.labelInterstitialText.text = text;
        }
    }
    
    self.viewDidLoadOneTime = YES;
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

//interstitialPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATKitPlacement>*)interstitialPlacement{
    return [AATKit getPlacementWithName:interstitialPlacement] ?:
    [AATKit createPlacementWithName:interstitialPlacement andType:AATKitFullscreen andStatisticsDelegate:self];
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}

@end
