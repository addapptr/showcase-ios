//
//  AATCMPBarButtonItem.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 16/10/18.
//  Copyright © 2018 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AATCMPBarButtonItem : UIBarButtonItem
- (instancetype)initWithViewController:(UIViewController*)viewController;
@end

NS_ASSUME_NONNULL_END
