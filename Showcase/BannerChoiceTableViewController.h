//
//  BannerChoiceTableViewController.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 17.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerChoiceTableViewController : UITableViewController
@end
