//
//  NativeAdTableViewController.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 30.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

@interface NativeAdTableViewController : UITableViewController <UIToolbarDelegate>


@end
