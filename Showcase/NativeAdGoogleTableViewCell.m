//
//  NativeAdGoogleTableViewCell.m
//  Showcase
//
//  Created by Mahmoud Amer on 13.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h>
#import "NativeAdGoogleTableViewCell.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface NativeAdGoogleTableViewCell()

@property (weak, nonatomic) IBOutlet GADNativeAdView *adContainerView;
@property GADMediaView* googleMediaView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *viewForAdImageView;
@property (weak, nonatomic) IBOutlet UIImageView *adImageView;
@property CGRect adImageViewFrame;
@property (weak, nonatomic) IBOutlet UILabel *sponsoredLabel;
@property (weak, nonatomic) IBOutlet UILabel *callToActionButton;
@property (weak, nonatomic) IBOutlet UIView *brandingLogoView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@end

@implementation NativeAdGoogleTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.descriptionLabel.text = @"";
    self.sponsoredLabel.text = @"";
    
    self.callToActionButton.text = @"";
}

- (void)setAd:(NSObject<AATKitNativeAd> *)ad
{
    [self updateContentForAd:ad];
}

- (void)clearAd {
    self.ad = nil;
}

//In this method the outlets of the storyboard get the content of the native ad container.
//In most cases there is a title, a description, a main image, an icon image and a "call to action" (the text that is mostly used for a button).
//Always check if the components of the ad handed over are != nil.
- (void)updateContentForAd:(NSObject<AATKitNativeAd>*)ad {
    
    //You always have to remove and set the tracking view.
    //The tracking view is the view that will link you to the website if you tap on it.
    if (self.ad) {
        [AATKit removeTrackingViewForNativeAd:self.ad];
    }
    
    //Set the value of the property ad of this class to the value of ad handed over from NativeAdTableViewController.m
    [self willChangeValueForKey:NSStringFromSelector(@selector(ad))];
    _ad = ad;
    [self didChangeValueForKey:NSStringFromSelector(@selector(ad))];
    
    //Set the tracking view for this native ad.
    if (!self.ad) {
        return;
    }
    
    [self setTrackingView];
    
    NSString* nativeAdTitle = ad ? [AATKit getNativeAdTitle:ad] : @"";
    [self changeTitleOfNativeAd:nativeAdTitle];
    
    //For most ad networks you have to make clear that this is a native ad by marking it with a label. In this case it says "Sponsored".
    NSString* nativeAdAdverstiser = ad ? [AATKit getNativeAdAdvertiser:ad] : nil;
    if ([nativeAdAdverstiser isEqual: @""] || nativeAdAdverstiser == nil) {
        nativeAdAdverstiser = @"Sponsored";
    }
    [self changeTextOfSponsoredLabel:nativeAdAdverstiser];
    
    NSString* nativeAdDescription = ad ? [AATKit getNativeAdDescription:ad] : @"";
    [self changeDescriptionOfNativeAd:nativeAdDescription];
    
    //The image is handed over as a URL.
    NSString* imageURL = ad? [AATKit getNativeAdImageURL:ad] : nil;
    if (imageURL != nil) {
        [self changeImageOfNativeAd:imageURL];
    } else {
        self.adImageView.image = nil;
    }
    //The icon image is handed over as a URL.
    NSString* iconURL = ad? [AATKit getNativeAdIconURL:ad] : nil;
    if (iconURL != nil) {
        [self changeIconOfNativeAd:iconURL];
    } else {
        self.iconImageView.image = nil;
    }
    //The branding logo is handed over as a view.
    UIImageView* brandingLogoImageView = ad ? [AATKit getNativeAdBrandingLogoView:ad] : nil;
    if (brandingLogoImageView != nil) {
        [self changeBrandingLogoOfNativeAd:brandingLogoImageView];
    }
    NSString* callToAction = ad ? [AATKit getNativeAdCallToAction:ad] : @"";
    if (callToAction != nil) {
        [self changeTextOfCallToAction:callToAction];
    }
}

-(void)setTrackingView {
    [AATKit setTrackingView:self.adContainerView forNativeAd:self.ad mainImageView:self.imageView iconView:self.iconImageView CTAView:nil];
}

-(void)changeTitleOfNativeAd:(NSString *)title{
//    ((UILabel *)self.adContainerView.headlineView).text = title;

    self.adContainerView.headlineView = self.titleLabel;
    self.titleLabel.text = title;
    [self.titleLabel sizeToFit];
}

-(void)changeDescriptionOfNativeAd:(NSString *)description{
//    ((UILabel *)self.adContainerView.bodyView).text = description;
    self.adContainerView.bodyView = self.descriptionLabel;
    self.descriptionLabel.text = description;
    [self.descriptionLabel sizeToFit];
}

-(void)changeImageOfNativeAd:(NSString *)imageURL{
    [self.googleMediaView removeFromSuperview];
    self.googleMediaView = nil;

    self.adImageView.image = nil;
    
    self.googleMediaView = [[GADMediaView alloc] initWithFrame:self.adImageView.bounds];
    self.adContainerView.mediaView = self.googleMediaView;
    [self.adImageView addSubview:self.googleMediaView];
}

-(void)changeBrandingLogoOfNativeAd:(UIImageView *)brandingLogoImageView{
    NSArray *viewsToRemove = [self.brandingLogoView subviews];
    for (UIImageView *view in viewsToRemove) {
        [view removeFromSuperview];
    }
    
    CGRect imageViewFrame = self.brandingLogoView.bounds;
    brandingLogoImageView.frame = imageViewFrame;
    
    [self.brandingLogoView addSubview:brandingLogoImageView];
}

-(void)changeTextOfCallToAction:(NSString *)callToAction{
    self.adContainerView.callToActionView = self.callToActionButton;
    self.callToActionButton.text = callToAction;
    self.callToActionButton.hidden = false;
}

-(void)changeTextOfSponsoredLabel:(NSString *)text{
    self.sponsoredLabel.text = text;
    [self.sponsoredLabel sizeToFit];
}

-(void)changeIconOfNativeAd:(NSString*)iconURL {
    self.iconImageView.image = nil;
    
    self.adContainerView.iconView = self.iconImageView;
    NSURL *url = [NSURL URLWithString:
                  iconURL];
    
    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession] downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        UIImage *downloadedImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:location]];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.iconImageView.image = downloadedImage;
        });
    }];
    
    [downloadPhotoTask resume];
}

@end

