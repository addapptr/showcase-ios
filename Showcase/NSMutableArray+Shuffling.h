//
//  NSMutableArray+Shuffling.h
//  Showcase
//
//  Created by Mahmoud Amer on 19.12.19.
//  Copyright © 2019 AddApptr GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffling)
- (void)shuffle;
-(NSMutableArray *)injectObjectsWithMax:(int)max objects:(NSArray *)objects;
-(NSMutableArray *)injectTwiceObjectsWithMax:(int)max;
@end
