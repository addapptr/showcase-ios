//
//  BannerChoiceTableViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 17.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "BannerChoiceTableViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "BannerChoiceCell.h"

@interface BannerChoiceTableViewController ()

@property NSArray* bannerFormats;

@end

@implementation BannerChoiceTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
    
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];

    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
    
    self.tableView.scrollEnabled = false;
    
    self.bannerFormats = [NSArray arrayWithObjects:@"Sticky Banner", @"Banner in Content-Feed", @"Multi-Size Banner", nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Layout tended to break because tab bar didn't load by the time the row heights were determined
    // Reloading table view in viewWillAppear fixes this
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BannerChoiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bannerChoiceCell"];
    
    [cell changeText:[self.bannerFormats objectAtIndex:indexPath.row]];
    [cell changeImageForIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ((self.tableView.frame.size.height - self.tabBarController.tabBar.frame.size.height - self.navigationController.navigationBar.frame.size.height - [[UIApplication sharedApplication]statusBarFrame].size.height) / 3);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"SegueBannerToStickyBanner" sender:self];
    }
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"SegueBannerToBannerInContentFeed" sender:self];
    }
    if (indexPath.row == 2) {
        [self performSegueWithIdentifier:@"SegueBannerToMultiSizeBanner" sender:self];
    }
}


@end
