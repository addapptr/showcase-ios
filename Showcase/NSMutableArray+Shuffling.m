//
//  NSMutableArray+Shuffling.m
//  Showcase
//
//  Created by Mahmoud Amer on 19.12.19.
//  Copyright © 2019 AddApptr GmbH. All rights reserved.
//

#import "NSMutableArray+Shuffling.h"

@implementation NSMutableArray (Shuffling)

- (void)shuffle
{
    NSUInteger count = [self count];
    for (uint i = 0; i < count - 1; ++i)
    {
        // Select a random element between i and end of array to swap with.
        int nElements = (int)count - i;
        int n = arc4random_uniform(nElements) + i;
        [self exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}

-(NSMutableArray *)injectObjectsWithMax:(int)max objects:(NSArray *)objects {
    int step = 2;
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:self];
    
    for (int i = 0; i < max; i ++) {
        if (step < tempArray.count) {
            [tempArray insertObject:[objects objectAtIndex:i] atIndex:step];
        }
        step = step + 3;
    }
    
    NSLog(@"old array : %@", self);
    NSLog(@"new array : %@", tempArray);
    return tempArray;
}

-(NSMutableArray *)injectTwiceObjectsWithMax:(int)max {
    
    NSArray *adWithExp =@[
        @{
            @"type": @"ad",
            @"ad": @""
        },
        @{
            @"type": @"exp",
            @"exp": @"Multi-size banner will be displayed above this cell"
        }
    ];
    
    int step = 2;
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:self];
    
    for (int i = 0; i < max; i ++) {
        if (step < tempArray.count) {
            [tempArray insertObject:[adWithExp objectAtIndex:0] atIndex:step];
            [tempArray insertObject:[adWithExp objectAtIndex:1] atIndex:step + 1];
        }
        step = step + 3;
    }
    
    NSLog(@"old array : %@", self);
    NSLog(@"new array : %@", tempArray);
    return tempArray;
}

@end
