//
//  main.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 15.08.17.
//  Copyright © 2017 Raphael-Alexander Berendes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
