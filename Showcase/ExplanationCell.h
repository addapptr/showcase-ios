//
//  ExplanationCell.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 04.09.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExplanationCell : UITableViewCell
-(void)changeText:(NSString*)text;

@end
