//
//  ExplanationCell.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 04.09.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "ExplanationCell.h"

@interface ExplanationCell()

@property (weak, nonatomic) IBOutlet UILabel *explanationLabel;

@end

@implementation ExplanationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.explanationLabel.text = @"";
}

-(void)changeText:(NSString*)text{
    
    self.explanationLabel.text = text;
    [self.explanationLabel sizeToFit];
}

@end
