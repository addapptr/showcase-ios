//
//  UIColor+AAT.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 12.10.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "UIColor+AAT.h"

@implementation UIColor (AAT)

+ (instancetype)aat_gray {
    return [UIColor colorWithRed:157.0f/255.0f
                           green:157.0f/255.0f
                            blue:157.0f/255.0f
                           alpha:1.0f];
}

+ (instancetype) aat_orange {  
    return [UIColor colorWithRed:249.0/255.0
                           green:151.0/255.0
                            blue:46.0/255.0
                           alpha:1.0];
}

@end
