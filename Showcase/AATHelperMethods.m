//
//  AATHelperMethods.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 03.04.18.
//  Copyright © 2018 AddApptr GmbH. All rights reserved.
//

#import "AATHelperMethods.h"

#import <UIKit/UIKit.h>

@implementation AATHelperMethods
    
+ (AATHelperMethods*)sharedInstance
{
    static AATHelperMethods *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
    
- (void)openAddApptrWebsite {
    NSURL* addapptrWebsite = [NSURL URLWithString:@"https://www.addapptr.com"];
    NSDictionary* options = [NSDictionary dictionary];
    [[UIApplication sharedApplication] openURL:addapptrWebsite options:options completionHandler:nil];
}

- (void)openWebsiteWithUrl:(NSString *)urlString {
    NSURL* addapptrWebsite = [NSURL URLWithString:urlString];
    NSDictionary* options = [NSDictionary dictionary];
    [[UIApplication sharedApplication] openURL:addapptrWebsite options:options completionHandler:nil];
}
    
@end
