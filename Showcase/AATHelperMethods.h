//
//  AATHelperMethods.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 03.04.18.
//  Copyright © 2018 AddApptr GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AATHelperMethods : NSObject
+ (AATHelperMethods*)sharedInstance;
- (void)openAddApptrWebsite;
- (void)openWebsiteWithUrl:(NSString *)urlString;
    
@end
