//
//  AATCMPBarButtonItem.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 16/10/18.
//  Copyright © 2018 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h>

#import "AATCMPBarButtonItem.h"
#import "AppDelegate.h"

@interface AATCMPBarButtonItem()
@property UIViewController* viewController;
@end

@implementation AATCMPBarButtonItem

- (instancetype)initWithViewController:(UIViewController*)viewController
{
    self = [super init];
    self.viewController = viewController;
    self.target = self;
    self.action = @selector(presentCMP);
    self.title = @"\u2699";
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:18.0];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:font, NSFontAttributeName, nil];
    [self setTitleTextAttributes:dict forState:UIControlStateNormal];
    return self;
}

- (void)presentCMP
{
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate.consent editConsent:self.viewController];
}

@end
