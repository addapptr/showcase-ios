//
//  NativeAdCell.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 25.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

@interface NativeAdCell : UITableViewCell

@property (nonatomic) NSObject<AATKitNativeAd>* ad;

@property (weak, nonatomic) IBOutlet UILabel *explanationLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;

- (void)updateContentForAd:(NSObject<AATKitNativeAd>*)ad;
- (void)clearAd;
@end
