//
//  NativeAdTableViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 25.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this

#import "NativeAdTableViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "AATPlacementNames.h"
#import "ExplanationCell.h"
#import "NativeAdCell.h"
#import "BannerContentCell.h"
#import "NSIndexPath+AAT.h"
#import "NSUserDefaults+AAT.h"
#import "NSMutableArray+Shuffling.h"
#import "NativeAdGoogleTableViewCell.h"
#import "NativeAdElement.h"

const int NATIVE_ADS_COUNT = 4;
const int CELL_HEIGHT = 186;

@interface NativeAdTableViewController () <AATKitStatisticsDelegate>
@property UIRefreshControl* refreshControl;

@property (nonatomic, getter=nativeAdPlacement, readonly) NSObject<AATKitPlacement>* nativeAdPlacement; //The placement for the multi size banner. The getter is defined further down
@property (getter=isLoading) BOOL loading; //Used for method loadAdIfNeeded to only load one ad at a time
@property (getter=isNewsLoading) BOOL newsLoading;

@property NSMutableArray* dataSourceArray;
@property NSArray *explanations;

@property NSMutableArray<NativeAdElement *> *adsIndexPaths;
@property int fetchedAdsCount;
@property int adsOffset;

@end

@implementation NativeAdTableViewController
@synthesize refreshControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
    
    // Refresh Control
    self.refreshControl = [[UIRefreshControl alloc] init];
    if (@available(iOS 10, *)) {
        self.tableView.refreshControl = self.refreshControl;
    } else {
        [self.tableView addSubview:self.refreshControl];
    }
    [self.refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
        
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    
    [self displayAddaptrLogoWithWebsiteRedirect];
    
    NSNotificationCenter* notificationCenter = NSNotificationCenter.defaultCenter;
    
    //React on having a native ad
    [notificationCenter addObserverForName:@"AATKitHaveAd" object:self.nativeAdPlacement queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        //Set BOOL loading to NO, so that the next execution of the method loadAdIfNeeded will lead to loading an ad.
        self.loading = NO;
        //Store loaded nativ ad in nativeAd
        NSObject<AATKitNativeAd>* nativeAd = [AATKit getNativeAdForPlacement:self.nativeAdPlacement];
        int adIndex = 2;
        int i = 0;
        for (NativeAdElement *element in self.adsIndexPaths) {
            if (!element.nativeAd){
                self.adsIndexPaths[i].nativeAd = nativeAd;
                self.fetchedAdsCount ++;
                for (UITableViewCell *cell in self.tableView.visibleCells) {
                    if ([self.tableView indexPathForCell:cell] == self.adsIndexPaths[i].indexPath) {
                        [self.tableView reloadRowsAtIndexPaths:@[self.adsIndexPaths[i].indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                }
                
                if (self.fetchedAdsCount < NATIVE_ADS_COUNT)
                    [self loadAdIfNeeded];
                break;
            }
            i ++;
            adIndex += self.adsOffset;
        }
    }];
    
    //React on AATKit not being able to load an ad for the nativeAdPlacement with trying to load an ad again
    [notificationCenter addObserverForName:@"AATKitHaveNoAds" object:self.nativeAdPlacement queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        self.loading = NO;
        if (self.fetchedAdsCount < NATIVE_ADS_COUNT)
            [self loadAdIfNeeded];
    }];
    
    [notificationCenter addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        if ((self.fetchedAdsCount < NATIVE_ADS_COUNT) && !self.isLoading)
            [self loadAdIfNeeded];
        
        if ([self isViewLoaded] && self.view.window) {
            [self.tableView reloadData];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self prepareDataSource];
    [self prepareDataSource];
}

- (void)prepareDataSource {
    [self clearNativeAds];
    self.dataSourceArray = [[NSMutableArray alloc] init];
    self.adsIndexPaths = [[NSMutableArray alloc] init];
    self.fetchedAdsCount = 0;
    self.adsOffset = (int)(UIScreen.mainScreen.bounds.size.height / CELL_HEIGHT) + 1;
    
    [self performSelectorInBackground:@selector(loadNews) withObject:nil];
}

- (void)refreshTableView:(UIRefreshControl*)refreshControl
{
    if (self.loading || self.isNewsLoading) {
        [self.refreshControl endRefreshing];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Loading"
                                                                       message:@"Still loading!"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        return;
    }

    [self clearNativeAds];
    [self performSelectorInBackground:@selector(loadNews) withObject:nil];
}

- (void)endRefreshing
{
    [self.refreshControl endRefreshing];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)displayAddaptrLogoWithWebsiteRedirect {
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];
    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

//nativeAdPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATKitPlacement>*)nativeAdPlacement{
    return [AATKit getPlacementWithName:nativeAdPlacement] ?:
    [AATKit createPlacementWithName:nativeAdPlacement andType:AATKitNativeAd andStatisticsDelegate:self];
}

//This method ensures that only one native ad will be loaded at a time
- (void) loadAdIfNeeded {
    if (self.isLoading && !(self.fetchedAdsCount < NATIVE_ADS_COUNT)) {
        [self endRefreshing];
        return;
    }
    self.loading = YES;
    // Force load will load for the selected placement, even if there's
    // already a load in progress for the placement
    [AATKit reloadPlacement:self.nativeAdPlacement forceLoad:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    for (NativeAdElement *element in self.adsIndexPaths) {
        if (element.indexPath == indexPath) {
            if (!element.nativeAd)
                return [[UITableViewCell alloc] init];
            if ([self isGoogleNativeAd:element.nativeAd]) {
                NativeAdGoogleTableViewCell *googleCell = [tableView dequeueReusableCellWithIdentifier:@"nativeAdGoogleCell" forIndexPath:indexPath];
                [AATKit setViewController:self forNativeAd:element.nativeAd];
                if (!googleCell.ad) {
                    [googleCell updateContentForAd:element.nativeAd];
                }
                if (!NSUserDefaults.standardUserDefaults.hideExplanations) {
                    googleCell.explanationLabel.text = element.explanation;
                    [googleCell.bottomSeparatorView setHidden:NO];
                } else {
                    googleCell.explanationLabel.text = @"";
                    [googleCell.bottomSeparatorView setHidden:YES];
                }
                return googleCell;
            } else {
                NativeAdCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nativeAdCell" forIndexPath:indexPath];
                if (!cell.ad) {
                    [AATKit setViewController:self forNativeAd:element.nativeAd];
                    [cell updateContentForAd:element.nativeAd];
                }
                if (!NSUserDefaults.standardUserDefaults.hideExplanations) {
                    cell.explanationLabel.text = element.explanation;
                    [cell.bottomSeparatorView setHidden:NO];
                } else {
                    cell.explanationLabel.text = @"";
                    [cell.bottomSeparatorView setHidden:YES];
                }
                return cell;
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [self.dataSourceArray objectAtIndex:indexPath.row];
    NSString *type = [data objectForKey:@"type"];
    
    if ([type isEqualToString:@"exp"]) {
        ExplanationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"explanationCell"];
        
        [cell changeText:[data objectForKey:@"exp"]];
        return cell;
    } else if ([type isEqualToString:@"news"]) {
        BannerContentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"bannerContentCell" forIndexPath:indexPath];
        cell.labelBannerContentCell.text = [data objectForKey:@"body"];
        cell.titleLabel.text = [data objectForKey:@"title"];
        cell.newsImageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
        
        return cell;
    }
    return [[UITableViewCell alloc] init];
}

- (BOOL)isGoogleNativeAd:(NSObject<AATKitNativeAd>*)ad
{
    if ([AATKit getNativeAdNetwork:ad] == AATDFP || [AATKit getNativeAdNetwork:ad] == AATAdMob || [AATKit getNativeAdNetwork:ad] == AATAdX) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[BannerContentCell class]]) {
        [[AATHelperMethods sharedInstance] openWebsiteWithUrl:@"https://www.addapptr.com/news-and-pr"];
    }
}

-(void)loadNews {
    if (self.isNewsLoading) {
        return;
    }
    self.newsLoading = YES;
    NSString * filePath =[[NSBundle mainBundle] pathForResource:@"NewsMore" ofType:@"json"];
    
    NSError * error;
    NSString* fileContents =[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    
    if(error)
    {
        NSLog(@"Error reading file: %@",error.localizedDescription);
    }
    
    
    NSDictionary *newsDictionary = (NSDictionary *)[NSJSONSerialization
                                                    JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                                    options:0 error:NULL];
        
    [self performSelectorOnMainThread:@selector(finishLoadingNews:) withObject:[newsDictionary objectForKey:@"news"] waitUntilDone:NO];
}

-(void)finishLoadingNews:(NSArray *)newsArray {
    self.newsLoading = NO;
    // append news array to data source
    self.dataSourceArray = [NSMutableArray arrayWithArray:newsArray];
    [self fillExplanationsArray];
}

-(void)fillExplanationsArray {
    self.explanations = @[
        @"You have to make clear that native ads are ads by highlighting it in some way, like labeling it as „sponsored“, putting the logo of the ad network in it or making changes in color.",
        @"You have to make clear that native ads are ads by highlighting it in some way, like labeling it as „sponsored“, putting the logo of the ad network in it or making changes in color.",
        @"Every ad network has a special guideline for native ads."
    ];
    [self reserveAdsIndexPaths];
}

- (void)reserveAdsIndexPaths {
    int adIndex = 2;
    for (int i = 0; i < NATIVE_ADS_COUNT; i ++ ) {
        NativeAdElement *cellElement = [NativeAdElement alloc];
        NSIndexPath *path = [NSIndexPath indexPathForRow:adIndex inSection:0];
        if (self.explanations.count > i)
            cellElement.explanation = self.explanations[i];
        else
            cellElement.explanation = self.explanations[0];
        cellElement.indexPath = path;
        cellElement.nativeAd = nil;
        [self.adsIndexPaths addObject:cellElement];
        adIndex += self.adsOffset;
    }
    [self.tableView reloadData];
    [self loadAdIfNeeded];
    [self endRefreshing];
}

-(void)clearNativeAds {
    self.fetchedAdsCount = 0;
    [self.dataSourceArray removeAllObjects];
    [self.adsIndexPaths removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}

@end
