//
//  NSUserDefaults+AAT.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 26.09.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (AAT)

@property (readonly) BOOL hideExplanations;
@property (readonly) BOOL gdprConsentObtainedByUser;
@property (readonly) BOOL gdprConsentValueSetByUser;


@end
