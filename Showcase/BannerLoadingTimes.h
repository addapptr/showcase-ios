//
//  TimeToLoadBanner.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 18.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#ifndef TimeToLoadBanner_h
#define TimeToLoadBanner_h

//Set loading times for placements to set them globally easier.
//Loading time is not allowed to be under 30 seconds.
static int const stickyBannerLoadingTime = 30;
static int const bannerInContentFeedLoadingTime = 30;

#endif /* TimeToLoadBanner_h */
