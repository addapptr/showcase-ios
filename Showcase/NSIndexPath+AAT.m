//
//  NSIndexPath+AAT.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 23.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSIndexPath+AAT.h"

//Class that defines an extension for NSIndexPath for being able to select the next or previous row of an index path in table view.
@implementation NSIndexPath (AAT)

- (NSIndexPath *)nextRow
{
    return [NSIndexPath indexPathForRow:self.row - 1 inSection:self.section];
}

- (NSIndexPath *)previousRow
{
    return [NSIndexPath indexPathForRow:self.row + 1 inSection:self.section];
}

@end
