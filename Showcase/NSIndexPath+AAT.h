//
//  NSIndexPath+AAT.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 23.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (AAT)
@property (nullable, readonly) NSIndexPath* previousRow;
@property (nullable, readonly) NSIndexPath* nextRow;
@end
