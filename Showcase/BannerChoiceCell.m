//
//  BannerChoiceCell.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 10.10.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "BannerChoiceCell.h"

@interface BannerChoiceCell()

@property (weak, nonatomic) IBOutlet UILabel *labelBannerChoiceCell;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBannerChoiceCell;

@end

@implementation BannerChoiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.labelBannerChoiceCell.text = @"";
}

-(void)changeText:(NSString*)text{
    
    CGRect screenRect   = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if (screenWidth == 320.0) {
        self.labelBannerChoiceCell.font = [UIFont systemFontOfSize:14];
    } else {
        self.labelBannerChoiceCell.font = [UIFont systemFontOfSize:17];
    }
    
    self.labelBannerChoiceCell.text = text;
}

-(void)changeImageForIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            self.imageViewBannerChoiceCell.image = [UIImage imageNamed:@"BannerChoiceSticky"];
            break;
        case 1:
            self.imageViewBannerChoiceCell.image = [UIImage imageNamed:@"BannerChoiceContentFeed"];
            break;
        case 2:
            self.imageViewBannerChoiceCell.image = [UIImage imageNamed:@"BannerChoiceMultiSize"];
            break;
        default:
            break;
    }
}

@end
