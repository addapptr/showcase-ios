//
//  NativeAdElement.m
//  Showcase
//
//  Created by Mahmoud Amer on 14.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import "NativeAdElement.h"

@implementation NativeAdElement

-(id)initWithPath:(NSIndexPath *)path explanation:(NSString *)explanation {
    self = [super init];
    if (self) {
        self.indexPath = path;
        self.explanation = explanation;
    }
    return self;
}

@end
