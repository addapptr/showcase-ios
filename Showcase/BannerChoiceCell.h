//
//  BannerChoiceCell.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 10.10.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerChoiceCell : UITableViewCell

-(void)changeText:(NSString*)text;
-(void)changeImageForIndexPath:(NSIndexPath*)indexPath;

@end
