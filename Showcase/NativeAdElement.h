//
//  NativeAdElement.h
//  Showcase
//
//  Created by Mahmoud Amer on 14.01.20.
//  Copyright © 2020 AddApptr GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AATKit/AATKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NativeAdElement : NSObject
@property NSIndexPath *indexPath;
@property NSString *explanation;
@property (nullable) NSObject<AATKitNativeAd> *nativeAd;
-(id)initWithPath:(NSIndexPath *)path explanation:(NSString *)explanation;
@end

NS_ASSUME_NONNULL_END
