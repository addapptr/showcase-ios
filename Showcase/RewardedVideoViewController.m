//
//  RewardedVideoViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 17.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this

#import "RewardedVideoViewController.h"

#import "AATCMPBarButtonItem.h"
#import "AATHelperMethods.h"
#import "AATPlacementNames.h"
#import "NSUserDefaults+AAT.h"

@interface RewardedVideoViewController () <AATKitStatisticsDelegate>

@property (assign, nonatomic, getter=rewardedVideoPlacement) NSObject<AATKitPlacement>* rewardedVideoPlacement; //The placement for the rewarded video. The getter is defined further down

@property (weak, nonatomic) IBOutlet UILabel *labelRewardedVideoText;
@property (weak, nonatomic) IBOutlet UILabel *labelRewardedVideoCoins;
@property (weak, nonatomic) IBOutlet UILabel *labelRewardedVideoMessage;
@property (weak, nonatomic) IBOutlet UIButton *buttonRewardedVideo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceIOS11;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceNotIOS11;


@property NSUInteger coins; //This value will be incremented every time a rewarded video has been watched

@property NSUserDefaults *defaults;
@property BOOL hideExplanations;

@end

@implementation RewardedVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[AATCMPBarButtonItem alloc] initWithViewController:self];
    
    //Set view controller for placement.
    [AATKit setPlacementViewController:self forPlacement:self.rewardedVideoPlacement];
    
    self.defaults = NSUserDefaults.standardUserDefaults;
    
    UIImage *img = [UIImage imageNamed:@"AddApptrLogoTitleView"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgView setImage:img];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToAddApptrWebsite:)];
    [imgView setUserInteractionEnabled:YES];
    [imgView addGestureRecognizer:tapGestureRecognizer];
    
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    self.navigationItem.titleView = imgView;
    
    self.labelRewardedVideoMessage.text = @"";
    
    self.coins = 0;
    NSString* rewardedVideoCoinsText = [NSString stringWithFormat:@"Coins: %lu", (unsigned long)self.coins];
    self.labelRewardedVideoCoins.text = rewardedVideoCoinsText;
    
    NSNotificationCenter* notificationCenter = NSNotificationCenter.defaultCenter;
    //React on AATKitUserEarnedIncentiveOnPlacement callback of AATKit in AppDelegate.m
    //In this example the coins are incremented by 50 each time
    [notificationCenter addObserverForName:@"AATKitHaveReward" object:self.rewardedVideoPlacement queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        self.coins = self.coins + 50;
        self.labelRewardedVideoCoins.text = [NSString stringWithFormat:@"Coins: %lu", (unsigned long)self.coins];
        self.labelRewardedVideoMessage.text = @"You earned 50 coins!";
    }];
    
    //React on AATKitHaveAd callback of AATKit in AppDelegate.m
    [notificationCenter addObserverForName:@"AATKitHaveAd" object:self.rewardedVideoPlacement queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self rewardedVideoReady];
    }];
    
    [notificationCenter addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        self.hideExplanations = self.defaults.hideExplanations;
        
        if (!self.hideExplanations) {
            NSString* rewardedVideoText = @"Tap the button to see a rewarded video.\n\nWhen a rewarded video is displayed, you can not use the app until the rewarded video has finished.\n\nAfter watching the ad entirely, a reward in the context of the app is earned. In this example you get a reward of 50 coins.";
            self.labelRewardedVideoText.textAlignment = NSTextAlignmentLeft;
            self.labelRewardedVideoText.text = rewardedVideoText;
            
        } else {
            NSString* rewardedVideoText = @"Tap the button to see a rewarded video.";
            self.labelRewardedVideoText.textAlignment = NSTextAlignmentCenter;
            self.labelRewardedVideoText.text = rewardedVideoText;
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [AATKit startPlacementAutoReload:self.rewardedVideoPlacement];
    
    self.hideExplanations = self.defaults.hideExplanations;
    
    if (!self.hideExplanations) {
        NSString* rewardedVideoText = @"Tap the button to see a rewarded video.\n\nWhen a rewarded video is displayed, you can not use the app until the rewarded video has finished.\n\nAfter watching the ad entirely, a reward in the context of the app is earned. In this example you get a reward of 50 coins.";
        self.labelRewardedVideoText.textAlignment = NSTextAlignmentLeft;
        self.labelRewardedVideoText.text = rewardedVideoText;
    } else {
        NSString* rewardedVideoText = @"Tap the button to see a rewarded video.";
        self.labelRewardedVideoText.textAlignment = NSTextAlignmentCenter;
        self.labelRewardedVideoText.text = rewardedVideoText;
    }
    [self checkPlacementStatus];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.labelRewardedVideoMessage.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goToAddApptrWebsite:(UITapGestureRecognizer *)recognizer
{
    [[AATHelperMethods sharedInstance] openAddApptrWebsite];
}

//rewardedVideoPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATKitPlacement>*)rewardedVideoPlacement{
    return [AATKit getPlacementWithName:rewardedVideoPlacement] ?:
    [AATKit createRewardedVideoPlacement:rewardedVideoPlacement andStatisticsDelegate:self];
}

- (IBAction)showRewardedVideo:(UIButton *)sender {
    [AATKit showPlacement:self.rewardedVideoPlacement];
    [self checkPlacementStatus];
}

-(void)checkPlacementStatus {
    if ([AATKit haveAdForPlacement:self.rewardedVideoPlacement]) {
        [self rewardedVideoReady];
    } else {
        [self rewardedVideoLoading];
    }
}

-(void)rewardedVideoReady {
    [self.buttonRewardedVideo setTitle:@"Show Rewarded Video" forState:UIControlStateNormal];
    self.labelRewardedVideoMessage.text = @"Rewarded video is ready";
}

-(void)rewardedVideoLoading {
    [self.buttonRewardedVideo setTitle: @"Loading Rewarded Video..." forState:UIControlStateNormal];
    self.labelRewardedVideoMessage.text = @"";
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}
@end
