//
//  UIColor+AAT.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 12.10.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface UIColor (AAT)
+ (instancetype) aat_gray;
+ (instancetype) aat_orange;
@end
NS_ASSUME_NONNULL_END
