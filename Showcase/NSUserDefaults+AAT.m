//
//  NSUserDefaults+AAT.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 26.09.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "NSUserDefaults+AAT.h"

@implementation NSUserDefaults (AAT)

- (BOOL)hideExplanations
{
    return ![self boolForKey:@"developer_info"];
}

- (BOOL)gdprConsentObtainedByUser
{
    return [self boolForKey:@"gdpr_consent"];
}

- (BOOL)gdprConsentValueSetByUser
{
    return [self boolForKey:@"gdpr_consent_value_set_by_user"];
}

@end
