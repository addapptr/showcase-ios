//
//  AATPlacementNames.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 16.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

//Class for storing names of placements in constants, so that spelling mistakes are reduced
#ifndef AATPlacementNames_h
#define AATPlacementNames_h

static NSString* const startupInterstitialPlacement = @"StartUpInterstitialPlacement";
static NSString* const interstitialPlacement = @"InterstitialPlacement";
static NSString* const rewardedVideoPlacement = @"RewardedVideoPlacement";
static NSString* const stickyBannerPlacement = @"StickyBannerPlacement";
static NSString* const bannerInContentFeedPlacement = @"BannerInContentFeedPlacement";
static NSString* const inFeedBannerPlacement = @"InFeedBannerPlacement";
static NSString* const nativeAdPlacement = @"NativeAdPlacement";

#endif /* AATPlacementNames_h */
