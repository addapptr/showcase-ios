//
//  StartupInterstitialViewController.h
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 15.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

@interface StartupInterstitialViewController : UIViewController
+(StartupInterstitialViewController*)sharedInstance;
@end
