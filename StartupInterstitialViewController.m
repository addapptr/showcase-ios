//
//  StartupInterstitialViewController.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 15.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import <AATKit/AATKit.h> //If you add AATKit via cocoapods, you have to import it like this
#import "StartupInterstitialViewController.h"
#import "AATPlacementNames.h"
#import "InterstitialViewController.h"
#import "AppDelegate.h"

@interface StartupInterstitialViewController () <AATKitStatisticsDelegate>

@property (assign, nonatomic, getter=startupInterstitialPlacement) NSObject<AATKitPlacement>* startupInterstitialPlacement; //The placement for the startup interstitial. The getter is defined further down

@property (weak, nonatomic) IBOutlet UILabel *labelCount;
@property (weak, nonatomic) IBOutlet UIView *hideView;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;

@property int seconds;
@property NSTimer* timerCountdown;

@property BOOL performedSegue;

@end

//This class is used to load and display an interstitial after the (optional) LaunchScreen - anyways before the content of the app - and proceed to the view controller that should be displayed first in the app.
//I worked with a timer to implement this behavior, because I don't want the user to wait more than five seconds after the LaunchScreen before they can see the content of the app.
@implementation StartupInterstitialViewController

+ (StartupInterstitialViewController*)sharedInstance
{
    static StartupInterstitialViewController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary* mainBundle = [[NSBundle mainBundle] infoDictionary];
    NSString* versionNumber = [mainBundle objectForKey:@"CFBundleShortVersionString"];
    self.labelVersion.text = [NSString stringWithFormat:@"Version: %@", versionNumber];
    
    //Set view controller for placement.
    [AATKit setPlacementViewController:self forPlacement:self.startupInterstitialPlacement];
    
    //Start loading a placement as soon as possible
    [AATKit startPlacementAutoReload:self.startupInterstitialPlacement];
    
    self.seconds = 5;
    self.labelCount.text = [NSString stringWithFormat:@"%i seconds", self.seconds];
    
    self.timerCountdown = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(updateCountdown) userInfo:nil repeats: YES];
    [self.timerCountdown fire];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    
    //React on AATKitResumeAfterAd callback of AATKit in AppDelegate.m with making a segue.
    [notificationCenter addObserverForName:@"AATKitResumeAfterAd" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        if (!self.performedSegue) {
            [self.timerCountdown invalidate];
            [AATKit setPlacementViewController:nil forPlacement:self.startupInterstitialPlacement];
            [self trySegue];
        }
    }];
}

- (void)updateCountdown {
    self.labelCount.text = [NSString stringWithFormat:@"%i seconds", self.seconds];
    
    if (self.isViewLoaded && self.view.window) {
        if ([AATKit showPlacement:self.startupInterstitialPlacement]) {
            InterstitialViewController.sharedInstance.startupInterstitialHasBeenDisplayed = YES;
            [AATKit stopPlacementAutoReload:self.startupInterstitialPlacement];
            [self.timerCountdown invalidate];
            self.hideView.hidden = false;
        }
    }
    
    if (self.seconds <= 0) {
        AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        if (delegate.cmpIsDisplaying.boolValue) {
            return;
        }
        //Make a segue if you think it's too much waiting time for the user.
        [AATKit stopPlacementAutoReload:self.startupInterstitialPlacement];
        [self.timerCountdown invalidate];
        [self trySegue];
        return;
    } else if (self.seconds == 1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            __weak typeof(self) weakSelf = self;
            weakSelf.labelCount.text = [NSString stringWithFormat:@"%i second", weakSelf.seconds];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            __weak typeof(self) weakSelf = self;
            weakSelf.labelCount.text = [NSString stringWithFormat:@"%i seconds", weakSelf.seconds];
        });

    }
    
    self.seconds--;
}

-(void)trySegue {
    
    if (!self.isViewLoaded && !self.view.window) {
        return;
    }
    
    self.performedSegue = YES;
    
    self.view.hidden = YES;
    
    UIApplication.sharedApplication.delegate.window.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//startupInterstitialPlacement will be created if it doesn't exist.
//This is advantageous, because you don't have to take care about accidently having no placement or duplicate placements
- (NSObject<AATKitPlacement>*)startupInterstitialPlacement{
    return [AATKit getPlacementWithName:startupInterstitialPlacement] ?:
    [AATKit createPlacementWithName:startupInterstitialPlacement andType:AATKitFullscreen andStatisticsDelegate:self];
}

#pragma mark - AATStatisticsDelegate
- (void)AATKitCountedAdSpace {
    NSLog(@"AATKitCountedAdSpace");
}
- (void)AATKitCountedRequestForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedRequestForNetwork %ld", (long)network);
}
- (void)AATKitCountedResponseForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedResponseForNetwork%ld", (long)network);
}
- (void)AATKitCountedImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedVImpressionForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedVImpressionForNetwork%ld", (long)network);
}
- (void)AATKitCountedClickForNetwork:(AATKitAdNetwork)network {
    NSLog(@"AATKitCountedClickForNetwork%ld", (long)network);
}

@end
