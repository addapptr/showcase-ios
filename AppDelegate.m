//
//  AppDelegate.m
//  Showcase
//
//  Created by Raphael-Alexander Berendes on 15.08.17.
//  Copyright © 2017 AddApptr GmbH. All rights reserved.
//

#import "AppDelegate.h"
#import "UIColor+AAT.h"
#import "AATPlacementNames.h"
#import "NSUserDefaults+AAT.h"
#import "StartupInterstitialViewController.h"

@interface AppDelegate () <AATKitDelegate, AATManagedConsentDelegate>
@property NSUserDefaults* userDefaults;
@end

//It is a clear way to initialize the AATKit in the AppDelegate and manage the callbacks with a notification center.
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.cmpIsDisplaying = [NSNumber numberWithBool:NO];
    /**
     * Register the default app settings for the first app start.
     * The "GDPR" and the "Developer Info" switch should be active by default.
     */
    self.userDefaults = NSUserDefaults.standardUserDefaults;
    [self.userDefaults registerDefaults:@{@"gdpr_consent": @YES}];

    /**
     * We need to enabled/disable ATTrackingManager requestTrackingAuthorizationWithCompletionHandler
     * based on a boolean from backend. to control it when apple force it's usage
     * No back-end for now, so will set it to NO for now
     */
    self.BACKEND_SWITCH_ATF_ON = [NSNumber numberWithBool:NO];
    // Initializing AATKit
    [AATKit debug:YES];
    // Setup AATConfiguration
    AATConfiguration* configuration = [[AATConfiguration alloc] init];
    configuration.delegate = self;
    configuration.defaultViewController = nil;
    
    // Define that this app needs the consent of the user regarding to the GDPR.
    configuration.consentRequired = YES;

    self.cmp = [[AATCMPGoogle alloc] init];
    self.consent = [[AATManagedConsent alloc] initWithDelegate:self cmp:self.cmp];
    configuration.consent = self.consent;

#if 0
    // Configure with testID to get test ads. Remove this line before publishing the app
    configuration.testModeAccountID = @797;
#endif
    // Initialize with configuration object
    [AATKit initializeWithConfiguration:configuration];
    self.window.tintColor = UIColor.aat_orange;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


# pragma mark - AATKitDelegate

// These are the callbacks of the AATKit.
// With a NSNotificationCenter you can react on callbacks for each ViewController
// individually.
- (void)AATKitHaveAd:(id)placement
{
    NSLog(@"Placement %@ is populated with Ads.", placement);
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"AATKitHaveAd" object:placement];
}

//Exclusively for havin an ad for a multi size banner
- (void)AATKitHaveAdOnMultiSizeBanner:(id)placement withView:(UIView *)placementView
{
    NSLog(@"MultiSizeBannerPlacement %@ is populated with Ads.", placement);
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"AATKitHaveMultiSizeAd" object:placement userInfo:@{@"multiSizeBannerView" : placementView}];
}

//Exclusively for having watched a rewarded video
- (void) AATKitUserEarnedIncentiveOnPlacement: (id) placement
{
    NSLog(@"Reward was earned by watching RewardedVideoPlacment");
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"AATKitHaveReward" object:placement];
}

- (void) AATKitNoAds:(id) placement
{
    //You don't need to implement this method if you are using auto reload ( [AATKit startPlacementAutoReload:placementName] )
    //instead of reload ( [AATKit reloadPlacement:placementName] ) for loading the ad, because auto reload will not
    //stop trying to load ads, even if it gets this callback. It will automatically reload until you stop it.

    NSLog(@"Placement %@ is not populated with Ads.", placement);
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"AATKitHaveNoAds" object:placement];
}

// The following callbacks enable you to react to:
// - an interstitial getting shown
// - getting linked to a website by tapping on a banner or native ad

// Gets called at the start
- (void) AATKitPauseForAd:(NSObject<AATKitPlacement> *)placement
{
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"AATKitPauseForAd" object:nil];
}

// Gets called when closing the ad/website
- (void) AATKitResumeAfterAd:(NSObject<AATKitPlacement> *)placement
{
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"AATKitResumeAfterAd" object:nil];
}


// MARK: AATManagedConsentDelegate
- (void)CMPNeedsUI:(nonnull AATManagedConsent *)managedConsent {
    NSLog(@"CMPNeedsUI");
//    needsToDisplayCMP
    if ([self.window.rootViewController isKindOfClass:[StartupInterstitialViewController class]]) {
        self.needsToDisplayCMP = YES;
    } else {
        self.cmpIsDisplaying = [NSNumber numberWithBool:YES];
        self.needsToDisplayCMP = NO;
        [managedConsent showIfNeeded:self.window.rootViewController];
    }
}

- (void)managedConsentCMPFailedToLoad:(nonnull AATManagedConsent *)managedConsent error:(nonnull NSString *)string {
    self.cmpIsDisplaying = [NSNumber numberWithBool:NO];
    NSLog(@"managedConsentCMPFailedToLoad error: %@", string);
}

- (void)managedConsentCMPFailedToShow:(nonnull AATManagedConsent *)managedConsent error:(nonnull NSString *)string {
    self.cmpIsDisplaying = [NSNumber numberWithBool:NO];
    NSLog(@"managedConsentCMPFailedToShow error: %@", string);
}

- (void)managedConsentCMPFinishedWithState:(AATManagedConsentState)state {
    NSLog(@"managedConsentCMPFinishedWithState %ld", (long)state);
    self.cmpIsDisplaying = [NSNumber numberWithBool:NO]; 
}


#pragma mark - Request Tracking Permission

- (void)requestTrackingPermission {
    if (self.BACKEND_SWITCH_ATF_ON.boolValue) {
        if (@available(iOS 14, *)) {
            [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
                switch (status) {
                    case ATTrackingManagerAuthorizationStatusNotDetermined:
                        break;
                    case ATTrackingManagerAuthorizationStatusRestricted:
                        break;
                    case ATTrackingManagerAuthorizationStatusDenied:
                        break;
                    case ATTrackingManagerAuthorizationStatusAuthorized:
                        break;
                }
            }];
        } else {
             // lower than iOS 14
        };
    }
}

@end
